class Vehicle {
  drive(): void {
    console.log('Chugga Chugga');
  }
  honk(): void {
    console.log('beep');
  }
}

class Cars extends Vehicle {
  drive(): void {
    console.log('vroom');
  }
}

const vehicle = new Vehicle();
const scross = new Cars();
scross.drive();
scross.honk();

vehicle.drive();
