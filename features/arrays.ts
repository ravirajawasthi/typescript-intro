const carMakers = ['ford', 'toyota', 'chevy'];

const carByMake = [['ikon'], ['corolla'], ['chevy']];

const someCard = carByMake.pop();
//someCard already has the correct type

const masks = []; // Array of anytype

const impDates: (Date | string)[] = [new Date(), '23-10-2004'];

type Drink = [string, boolean, number]; // 'color, isCarbonated, number'

const pepsi: Drink = ['brown', true, 50];
const sprite: Drink = ['clear', true, 50];
const tea: Drink = ['brown', false, 0];
const jeeru: Drink = ['dark brown', true, 30];

//edge case
const carStats = {
  engineSize: 2000,
  horsePower: 190,
};

type carStats = [number, number];
// these number become useless, becuase we dont know what they represent. In cases like this we will still use javaScript objects
