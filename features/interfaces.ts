interface Reportable {
  summary(): string;
}

const oldCivic = {
  name: 'Civic 2000',
  mfg: new Date('January 2000'),
  broken: false,
  summary() {
    return `
      Name: ${this.name}
      Manufacturing Date: ${this.mfg.toISOString()}
      Broken? :${this.broken}
    `;
  },
};

const Pepsi = {
  color: 'dark brown',
  carbonated: false,
  sugar: 40,
  summary() {
    return `
      Color: ${this.color}
      Carbonated? : ${this.carbonated}
      Sugar: ${this.sugar}gms
    `;
  },
};

const printSummary = (item: Reportable): void => {
  console.log(item.summary());
};

printSummary(oldCivic);
printSummary(Pepsi);
