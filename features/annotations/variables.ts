const apples: number = 5;
// not allowed
// apples = 3;
// apples = true;
let bananas: number = 134;
bananas = 1212;
// not allowed
//bananas = true

let hasEaten: boolean = true;
hasEaten = false;

const nullify: null = null;
const nothingMuch: undefined = undefined;

//build in types
const now: Date = new Date();

//Arrays
let colors: string[] = ['red', 'green', 'blue'];
let myNumbers: boolean[] = [true, false, true];

class Car {}

let car: Car = new Car();

let point: { x: number; y: number } = {
  x: 12,
  y: 15,
};

// function
const logNumber: (i: number) => void = (i) => {
  console.log(i);
};

// when to use annotations
// 1) when function returns anytype
const coords = '{x: 10, y: 20}';
const parsedCoords: { x: number; y: number } = JSON.parse(coords);

// 2) when we declare variable and initialize it later
let word = ['a', 'b', 'c'];

let foundWord: boolean;
for (let i = 0; i < word.length; i++) {
  if (word[i] === 'a') {
    foundWord = true;
  }
}

// 3) Variable whose type cannot be inferred
let numbers = [-10, -1, 12];
let numberAboveZero: boolean | number = false;

for (let index = 0; index < numbers.length; index++) {
  if (numbers[index] > 0) {
    numberAboveZero = numbers[index];
  }
}
