const add = (a: number, b: number): number => {
  return a + b;
};

function divide(a: number, b: number): number {
  return a / b;
}

const mul = function (a: number, b: number): number {
  return a * b;
};

const logger = (a: string): void => {
  console.log(a);
};

const throwError = (message: string): never => {
  throw new Error(message);
};

// this function now returns void
const sub = (a: number, b: number) => {
  a - b;
};
