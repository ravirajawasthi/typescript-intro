const profile = {
  name: 'alex',
  age: 20,
  coords: {
    lat: 0,
    lng: 20,
  },
  setAge(age: number): void {
    this.age = age;
  },
};
